const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

const pluginList = fs.readdirSync('./src/templates').map(function (name) {
    let isFile = fs.statSync('./src/templates/' + name).isFile();

    if (isFile) {
        if (!this.plugins) {
            this.plugins = [];
        }
        this.plugins.push(new HtmlWebpackPlugin({
            template: 'templates/' + name,
            filename: path.basename(name, path.extname(name)) + '.html'
        }));
    }

    return this;
}.bind(this))[0]['plugins'];


module.exports = {
    context: __dirname + '/src/',
    entry: {
        'main.js': './scripts/index.js',
        'main.css': './styles/index.scss'
    },
    output: {
        path: __dirname + '/build/',
        filename: '[name]'
    },
    module: {
        rules: [{
            test: /\.slim$/,
            use: ['html-loader', 'slim-lang-loader']
        }, {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                use: ['css-loader', 'sass-loader']
            })
        }, {
            test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/,
            use: [{
                loader: 'file-loader',
                options: {
                    name: 'assets/[path][name].[ext]'
                }
            }]
        }]
    },
    plugins: [
        new CleanWebpackPlugin(['build']),
        new ExtractTextPlugin({
            filename: 'main.css',
            allChunks: true
        })
    ].concat(pluginList)
};