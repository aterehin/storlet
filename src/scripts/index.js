import $ from 'jquery';
import bootstrap from 'bootstrap';
import datepicker from 'bootstrap-datepicker';

$(function() {
	(function () {
		let input = () => {
			return $('.form-group-material input').focus(function() {
				return $(this).closest('.form-group-material').addClass('has-value');
			}).blur(function() {
				setTimeout(() => {
					if (!this.value) {
						$(this).closest('.form-group-material').removeClass('has-value');
					}
					
				}, 10);
			})
		}
		$(function () {
			return input();
		});
	}).call(this);

	$('input[type="file"]').on('change', function(){
		var value = this.files[0] ? this.files[0].name : '';
        $(this).closest('.form-group-material').find('input[type="text"]').val(value);
	});

	$('.toggle-menu').on('click', function() {
		$(this).add('body').add('.block-header').toggleClass('active');
	});

	$('#datepicker').datepicker({
		autoclose: true,
		todayHighlight: true,
	});

	$('.toggle-slide-menu').on('click', function() {
		$(this).add('.block-header').add('.main').toggleClass('sidebar-mini');
	});


	(function(){
        var errors = [];

        function showErrors(){
            errors.forEach(function(input){
            	var $input = $(input),
					$wrapper = $input.closest('.form-group-material'),
					msg = $input.attr('valid-msg');

                $input.addClass('is-invalid');
                $wrapper.addClass('has-error');
                $wrapper.find('.invalid-feedback').remove();
                $wrapper.append('<div class="invalid-feedback">'+ msg +'</div>');
			});
        }

        $('.block-auth_form').on('submit', function(e){
            var inputs = $(this).find('input[valid-email]');

            errors = [];

            inputs.each(function(index, input){
                if(input.value !== '' && !/(.+)@(.+){2,}\.(.+){2,}/.test(input.value)) {
                    errors.push(input);
                }
            });

            if(errors.length) {
                e.preventDefault();
                showErrors();
            }
        });
	})();
});
